#!/bin/sh
set -e

ver=4
prio=900

# These commands have been moved to be slaves, but we need to remove the old
# alternatives for both rt4-extension-mergeusers and rt5-extension-mergeusers.
remove_now_slave_alternatives() {
    for cmd in rt-clean-merged-users rt-update-merged-users
    do
        # Skip if the alternative doesn't exist.
        update-alternatives --list $cmd || continue

        for target in $(update-alternatives --list $cmd)
        do
            update-alternatives --remove $cmd $target
        done
    done
}

alts() {
    bin=rt-merge-users
    bin_slave="rt-update-merged-users rt-clean-merged-users"
    man3_slave=RT::Extension::MergeUsers;

    bin_path=/usr/bin
    man3_path=/usr/share/man/man3

    slave=""

    for s in $bin_slave; do
        slave="$slave --slave $bin_path/$s $s \
            $bin_path/$s-$ver"
    done

    for s in $man3_slave; do
        slave="$slave --slave $man3_path/$s.3pm.gz $s.3pm.gz \
            $man3_path/$s-$ver.3pm.gz"
    done

    update-alternatives \
      --install $bin_path/$bin $bin $bin_path/$bin-$ver $prio \
      $slave
}

case "$1" in
    configure)
        # This should be removed in Trixie + 1
        remove_now_slave_alternatives

        alts
        ;;
    abort-upgrade)
        alts
        ;;
esac

#DEBHELPER#

